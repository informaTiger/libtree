/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

/**
 *
 * @author Thomas
 */
public class Test {
    
    public static void main(String[] args){
        Tree<Integer> tree = new Tree<>(3);
        tree.add(5);
        Node<Integer> node = tree.add(10);
        tree.add(27);
        
        tree.add(8, node);
        tree.add(56, node);
        tree.add(17, node);
        
        tree.remove(node);
        
        tree.print();
    }
}
