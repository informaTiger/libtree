/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

import java.io.PrintStream;
import java.util.NoSuchElementException;

/**
 *
 * @author Thomas
 */
public abstract class AbstractTree<T> implements ITree<T> {
    
    protected int size;
    
    public AbstractTree(){
        
    }
    
    @Override
    public int size() {
        return size;
    }

    @Override
    public int height() {
        if (isEmpty()){
            throw new IllegalStateException("Tree is empty");
        }
        return height(getRoot());
    }

    @Override
    public void print(PrintStream stream) {
        if (isEmpty()){
            throw new IllegalStateException("Tree is empty");
        }
        print(stream, getRoot(), "", true);
    }

    @Override
    public void preOrder(NodeFunction func) {
        preOrder(getRoot(), func);
    }

    @Override
    public void postOrder(NodeFunction func) {
        postOrder(getRoot(), func);
    }
    
    protected INode<T> internalSearch(T data){
        if (getRoot() == null){
            throw new NoSuchElementException("No such element");
        }
        
        INode<T> node = internalSearch(getRoot(), data);
        if (node == null){
            throw new NoSuchElementException("No such element");
        }
        return node;
    }
    
    protected INode<T> internalSearch(INode<T> node, T data){
        if (node == null){
            return null;
        } else if (node.getData() == data){
            return node;
        }
        
        for (int i = 0; i < node.getChildCount(); i++){
            INode<T> n = internalSearch(node.getChildAt(i), data);
            if (n != null){
                return n;
            }
        }
        return null;
    }
    
    private int height(INode<T> node){
        int h = 0;
        
        for (int i = 0; i < node.getChildCount(); i++){
            if (node.getChildAt(i) == null){
                continue;
            }
            h = Math.max(h, 1 + height(node.getChildAt(i)));
        }
        return h;
    }
    
    private void print(PrintStream out, INode<T> node, String indent, boolean last){
        if (node == null){
            return;
        }
        out.print(indent);
        
        if (last){
            out.print("└╴"); // U+2514 followed by U+2574
            indent += "  ";
        } else {
            out.print("├╴"); // U+251C followed by U+2574
            indent += "│ "; // U+2502 followed by space
        }
        out.println(node.getData());
        
        for (int i = 0; i < node.getChildCount(); i++){
            print(out, node.getChildAt(i), indent, i == node.getChildCount() - 1);
        }
    }
    
    private void preOrder(INode<T> node, NodeFunction func){
        func.callback(node);
        
        for (int i = 0; i < node.getChildCount(); i++){
            if (node.getChildAt(i) == null){
                continue;
            }
            preOrder(node.getChildAt(i), func);
        }
    }
    
    private void postOrder(INode<T> node, NodeFunction func){
        for (int i = 0; i < node.getChildCount(); i++){
            if (node.getChildAt(i) == null){
                continue;
            }
            postOrder(node.getChildAt(i), func);
        }
        
        func.callback(node);
    }
}
