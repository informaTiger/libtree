/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

import java.io.PrintStream;

/**
 *
 * @author Thomas
 */
public interface ITree<T> {
 
    public INode<T> getRoot();
    
    public INode<T> get(T data);
    
    public int size();
    
    public int height();
    
    public void print(PrintStream stream);
    
    public void preOrder(NodeFunction func);
    
    public void postOrder(NodeFunction func);
    
    default void print(){
        print(System.out);
    }
    
    default boolean isEmpty(){
        return size() == 0;
    }
}
