/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

/**
 *
 * @author Thomas
 */
public class Tree<T> extends AbstractTree<T> {
    
    private Node<T> root;
    
    public Tree(){
        
    }
    
    public Tree(T data){
        this.root = new Node(data);
    }

    public Node<T> add(T data){
        if (root == null){
            root = new Node<>(data);
            size++;
            return root;
        }
        Node<T> child = new Node(data, root);
        root.addChild(child);
        size++;
        
        return child;
    }
    
    public Node<T> add(T data, Node<T> parent){
        Node<T> child = new Node(data, parent);
        size++;
        
        if (parent != null){
            parent.addChild(child);
        }
        return child;
    }
    
    public void remove(Node<T> node){
        if (node.getParent() != null){
            node.getParent().removeChild(node);
            node.setParent(null);
        } else {
            root = null;
        }
        
        for (int i = node.getChildCount() - 1; i >= 0; i--){
            remove(node.getChildAt(i));
        }        
        size--;
    }
    
    @Override
    public Node<T> getRoot() {
        return root;
    }
    
    @Override
    public Node<T> get(T data){
        return (Node<T>)internalSearch(data);
    }
}
