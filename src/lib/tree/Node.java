/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Thomas
 */
public class Node<T> extends AbstractNode<T> {

    private Node<T> parent;
    
    private final List<Node<T>> children;
    
    public Node(){
        children = new ArrayList<>();
    }
    
    public Node(T data){
        this();
        this.data = data;
    }

    public Node(T data, Node<T> parent){
        this();
        this.data = data;
        this.parent = parent;
    }
    
    @Override
    public Node<T> getParent() {
        return parent;
    }
    
    public void setParent(Node<T> parent){
        this.parent = parent;
    }
    
    public void addChild(Node<T> node){
        children.add(node);
    }
    
    public void removeChild(Node<T> node){
        children.remove(node);
    }
    
    @Override
    public int getChildCount() {
        return children.size();
    }

    @Override
    public Node<T> getChildAt(int index) {
        return children.get(index);
    }
}
