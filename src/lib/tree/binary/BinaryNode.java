/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree.binary;

import lib.tree.AbstractNode;

/**
 *
 * @author Thomas
 */
public class BinaryNode<T> extends AbstractNode<T> {
    
    public static final int LEFT = 0;
    
    public static final int RIGHT = 1;

    private BinaryNode<T> parent;
    
    private BinaryNode<T> left;
    
    private BinaryNode<T> right;
    
    public BinaryNode(){
    
    }
    
    public BinaryNode(T data){
        this.data = data;
    }
    
    public BinaryNode(T data, BinaryNode<T> parent){
        this.data = data;
        this.parent = parent;
    }

    @Override
    public BinaryNode<T> getParent() {
        return parent;
    }
    
    public void setParent(BinaryNode<T> parent){
        this.parent = parent;
    }

    public BinaryNode<T> getLeft() {
        return left;
    }

    public void setLeft(BinaryNode<T> left) {
        this.left = left;
    }

    public BinaryNode<T> getRight() {
        return right;
    }

    public void setRight(BinaryNode<T> right) {
        this.right = right;
    }
    
    protected BinaryNode<T> remove(){
        if (isExternal()){
            attachNode(null);
        } else if (left == null){
            attachNode(right);
            right.setParent(parent);
        } else if (right == null){
            attachNode(left);
            left.setParent(parent);
        } else {
            BinaryNode<T> next = nextNode();
            BinaryNode<T> node = new BinaryNode<>(next.getData(), parent);
            
            next.remove();
            
            if (left != null){
                node.setLeft(left);
                left.setParent(node);
            }
            
            if (right != null){
                node.setRight(right);
                right.setParent(node);
            }
            attachNode(node);
            
            if (node.isRoot()){
                return node;
            }
        }
        return null;
    }

    @Override
    public int getChildCount() {
        if (left == null && right == null){
            return 0;
        } else if (left != null && right != null){
            return 2;
        }
        return 1;
    }

    @Override
    public BinaryNode<T> getChildAt(int index) {
        if (index < LEFT || index > RIGHT){
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        
        if (index == LEFT && left != null){
            return left;
        }
        return right;
    }
    
    private void attachNode(BinaryNode<T> node){
        if (parent == null){
            return;
        }
        
        if (parent.getLeft() == this){
            parent.setLeft(node);
        } else {
            parent.setRight(node);
        }
    }
    
    private BinaryNode<T> nextNode(){
        if (right == null){
            throw new IllegalArgumentException("No such element");
        }
        
        if (right.getLeft() == null){
            return right;
        }
        
        BinaryNode<T> left = right;
        while (left != null && left.getLeft() != null){
            left = left.getLeft();
        }
        
        if (left == null){
            throw new IllegalArgumentException("No such element");
        }
        return left;
    }
}
