/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree.binary;

import lib.tree.AbstractTree;
import lib.tree.NodeFunction;

/**
 *
 * @author Thomas
 */
public class BinaryTree<T> extends AbstractTree<T> {
    
    private BinaryNode<T> root;
    
    public BinaryTree(){
        
    }
    
    public BinaryTree(T data){
        this.root = new BinaryNode(data);
        size++;
    }

    @Override
    public BinaryNode<T> getRoot() {
        return root;
    }
    
    @Override
    public BinaryNode<T> get(T data) {
        return (BinaryNode<T>)internalSearch(data);
    }
    
    public BinaryNode<T> addLeft(BinaryNode<T> parent, T data){
        if (parent.getLeft() != null){
            parent.getLeft().setData(data);
            return parent.getLeft();
        }
        BinaryNode<T> left = new BinaryNode(data, parent);
        parent.setLeft(left);
        
        size++;
        
        return left;
    }
    
    public BinaryNode<T> addRight(BinaryNode<T> parent, T data){
        if (parent.getRight() != null){
            parent.getRight().setData(data);
            return parent.getRight();
        }
        BinaryNode<T> right = new BinaryNode(data, parent);
        parent.setRight(right);
        
        size++;
        
        return right;
    }
    
    public void removeNode(BinaryNode<T> node){
        BinaryNode<T> root = node.remove();
        if (root != null){
            this.root = root;
        }
        size--;
    }
    
    public void inOrder(NodeFunction func){
        inOrder(root, func);
    }
    
    private void inOrder(BinaryNode<T> node, NodeFunction func){
        if (node.getLeft() != null){
            inOrder(node.getLeft(), func);
        }
        
        func.callback(node);
        
        if (node.getRight() != null){
            inOrder(node.getRight(), func);
        }
    }
}
