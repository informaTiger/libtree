/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree.binary;

import java.util.NoSuchElementException;
import lib.tree.INode;

/**
 *
 * @author Thomas
 */
public class BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {
    
    public BinarySearchTree(){
        
    }
    
    public BinarySearchTree(T data){
        super(data);
    }

    @Override
    public BinaryNode<T> addLeft(BinaryNode<T> parent, T data) {
        throw new UnsupportedOperationException();
    }

    @Override
    public BinaryNode<T> addRight(BinaryNode<T> parent, T data) {
        throw new UnsupportedOperationException();
    }
    
    public BinaryNode<T> add(T data){
        return internalAdd(getRoot(), data);
    }
    
    private BinaryNode<T> internalAdd(BinaryNode<T> node, T data){
        if (data == node.getData()){
            node.setData(data);
            return node;
        } else if (data.compareTo(node.getData()) < 0){
            if (node.getLeft() == null){
                BinaryNode<T> left = new BinaryNode(data, node);
                node.setLeft(left);
                size++;
                
                return left;
            }
            return internalAdd(node.getLeft(), data);
        } else {
            if (node.getRight() == null){
                BinaryNode<T> right = new BinaryNode(data, node);
                node.setRight(right);
                size++;
                
                return right;
            }
            return internalAdd(node.getRight(), data);
        }
    }

    @Override
    protected BinaryNode<T> internalSearch(INode<T> node, T data) {
        if (node == null){
            throw new NoSuchElementException("No such element");
        }
        
        if (data == node.getData()){
            return (BinaryNode)node;
        } else if (data.compareTo(node.getData()) < 0){
            return internalSearch(((BinaryNode)node).getLeft(), data);
        }
        return internalSearch(((BinaryNode)node).getRight(), data);
    }    
}
