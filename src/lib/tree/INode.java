/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

/**
 *
 * @author Thomas
 */
public interface INode<T> {
    
    public INode<T> getParent();
    
    public T getData();
    
    public void setData(T data);
    
    public int getChildCount();
    
    public INode<T> getChildAt(int index);
    
    public int depth();
    
    default boolean isRoot(){
        return getParent() == null;
    }
    
    default boolean isExternal(){
        return getChildCount() == 0;
    }
}
