/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.tree;

/**
 *
 * @author Thomas
 */
public abstract class AbstractNode<T> implements INode<T> {
    
    protected T data;
    
    public AbstractNode(){
        
    }
    
    public AbstractNode(T data){
        this.data = data;
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public void setData(T data) {
        this.data = data;
    }

    @Override
    public int depth() {
        return depth(this, 1);
    }
    
    private int depth(INode<T> node, int d){
        if (node.getParent() == null){
            return d;
        }
        return depth(node.getParent(), d + 1);
    }
}
